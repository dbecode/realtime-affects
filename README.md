# realtime-affects

## Description

The scope of this system, is ensuring that the time of a specific affect runs even while a player is offline.

The server is calculating a new time for every specified affect every time a client connect and his affects are loaded.

Check CHANGELOG.md for updates.

## How to add a Real Time Affect

In order to add a real time affect, you can set the **bIsRealTime** parameter to **true** when calling the **AddAffect** function.

```cpp
ch->AddAffect(AFFECT_BLEND_POTION_5, 95, 200, 0, 180, 0, true, false, true); // The last "true" is bIsRealTime
```

You can take an example from `/source/server/game/testing/cmd_gm.cpp`

## Note for client source 

Note that adding the **python constant** inside **PythonApplicationModule.cpp** :

```cpp
PyModule_AddIntConstant(poModule, "ENABLE_REALTIME_AFFECTS", 1);
```

Is **NOT** mandatory at all. In fact, that constant is not used by the system itself.

Actually, that constant and the modification done in the **PythonNetworkStreamPhaseGame.cpp** file:

```cpp
#if defined(DBE_REAL_TIME_AFFECTS)
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "BINARY_NEW_AddAffect", Py_BuildValue("(iiiii)", rkElement.dwType, rkElement.bPointIdxApplyOn, rkElement.lApplyValue, rkElement.lDuration, rkElement.bIsRealTime));
#else 
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "BINARY_NEW_AddAffect", Py_BuildValue("(iiii)", rkElement.dwType, rkElement.bPointIdxApplyOn, rkElement.lApplyValue, rkElement.lDuration));
#endif // DBE_REAL_TIME_AFFECTS
```

have been made with the only purpose of making the client able to recognize real time affects.

This information could be useful for example to add custom tooltips for realtime affects.

In addition to that, if you decide to apply the above code, you will have to modify the **BINARY_NEW_AddAffect** function in your **game.py** file.


## Author

   - Author : Crash
   - Discord : Crash#9966

