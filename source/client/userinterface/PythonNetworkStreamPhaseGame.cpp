/*
* [CS-1]
* This part is optional:
* From here, you can modify the RecvAffectAddPacket function
* in order to pass the bIsRealTime parameter to the 
* BINARY_NEW_AddAffect function.
* Usefull for adding custom tooltips for real time affects.
*
* NOTE:
* If you do this, remember to change the BINARY_NEW_AddAffect function definition in game.py and uiaffectshower.py
*/

// Search for

bool CPythonNetworkStream::RecvAffectAddPacket()

// replace :

PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "BINARY_NEW_AddAffect", Py_BuildValue("(iiii)", rkElement.dwType, rkElement.bPointIdxApplyOn, rkElement.lApplyValue, rkElement.lDuration));

// with:

#if defined(DBE_REAL_TIME_AFFECTS)
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "BINARY_NEW_AddAffect", Py_BuildValue("(iiiii)", rkElement.dwType, rkElement.bPointIdxApplyOn, rkElement.lApplyValue, rkElement.lDuration, rkElement.bIsRealTime));
#else // DBE_REAL_TIME_AFFECTS
	PyCallClassMemberFunc(m_apoPhaseWnd[PHASE_WINDOW_GAME], "BINARY_NEW_AddAffect", Py_BuildValue("(iiii)", rkElement.dwType, rkElement.bPointIdxApplyOn, rkElement.lApplyValue, rkElement.lDuration));
#endif // DBE_REAL_TIME_AFFECTS