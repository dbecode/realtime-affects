// Search
	PyModule_AddIntConstant(poModule, "CAMERA_STOP", CPythonApplication::CAMERA_STOP);

// Add below
#if defined(DBE_REAL_TIME_AFFECTS)
	PyModule_AddIntConstant(poModule, "ENABLE_REALTIME_AFFECTS", 1);
#else 
	PyModule_AddIntConstant(poModule, "ENABLE_REALTIME_AFFECTS", 0);
#endif // DBE_REAL_TIME_AFFECTS
