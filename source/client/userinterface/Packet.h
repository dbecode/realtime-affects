// Search for:

typedef struct
{
	DWORD dwType;
	BYTE bPointIdxApplyOn;
	long lApplyValue;
	DWORD dwFlag;
	long lDuration;
	long lSPCost;

// Add below:

#if defined(DBE_REAL_TIME_AFFECTS)
	bool bIsRealTime;
#endif // DBE_REAL_TIME_AFFECTS