// Search for:

void SendAffectAddPacket(LPDESC d, CAffect* pkAff)

// Inside this function, search for:

	ptoc.elem.lSPCost = pkAff->lSPCost;

// Add Below:

#if defined(DBE_REAL_TIME_AFFECTS)
	ptoc.elem.bIsRealTime = pkAff->bIsRealTime;
#endif // DBE_REAL_TIME_AFFECTS


/*=============================================================================================================================*/


// Now search for this function:

void CHARACTER::SaveAffect()

// Inside this function, search for:

		p.elem.lSPCost = pkAff->lSPCost;

// Add below:

#if defined(DBE_REAL_TIME_AFFECTS)
		p.elem.bIsRealTime = pkAff->bIsRealTime;
#endif // DBE_REAL_TIME_AFFECTS


/*=============================================================================================================================*/


// Search for 

void CHARACTER::LoadAffect(DWORD dwCount, TPacketAffectElement* pElements)

// Inside this function, search for:

		pkAff->lDuration = pElements->lDuration;

// Replace with:

#if defined(DBE_REAL_TIME_AFFECTS)
		/*
		* [CS-1]
		* At this point the duration could be negative (Affect expired)
		* We could handle it here or let the ProcessAffect function event
		* manage the AffectStack and List.
		*/
		pkAff->lDuration = pElements->bIsRealTime ? pElements->lDuration - m_dwLogOffInterval : pElements->lDuration;
#else 
		pkAff->lDuration = pElements->lDuration;
#endif // DBE_REAL_TIME_AFFECTS

// now search for:

		pkAff->lSPCost = pElements->lSPCost;

// Add below:

#if defined(DBE_REAL_TIME_AFFECTS)
		pkAff->bIsRealTime = pElements->bIsRealTime;
#endif // DBE_REAL_TIME_AFFECTS


/*=============================================================================================================================*/


// Now search for this function:

bool CHARACTER::AddAffect(DWORD dwType, BYTE bApplyOn, long lApplyValue, DWORD dwFlag, long lDuration, long lSPCost, bool bOverride, bool IsCube)

// Replace with this one (The function is the same as before, just with one more optional parameter):

bool CHARACTER::AddAffect(
	DWORD dwType,
	BYTE bApplyOn,
	long lApplyValue,
	DWORD dwFlag,
	long lDuration,
	long lSPCost,
	bool bOverride,
	bool IsCube,
#if defined(DBE_REAL_TIME_AFFECTS)
	bool bIsRealTime
#endif //DBE_REAL_TIME_AFFECTS
)

// Inside this function, search for:

	pkAff->lSPCost = lSPCost;

// You will have 2 entries, add the following code for both of them:

#if defined(DBE_REAL_TIME_AFFECTS)
	pkAff->bIsRealTime = bIsRealTime;
#endif // DBE_REAL_TIME_AFFECTS
