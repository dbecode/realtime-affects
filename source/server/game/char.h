// Search for:

bool AddAffect(DWORD dwType, BYTE bApplyOn, long lApplyValue, DWORD dwFlag, long lDuration, long lSPCost, bool bOverride, bool IsCube = false);

// Replace with this one (The function is the same as before, just with one more optional parameter):

	bool AddAffect(
		DWORD dwType,
		BYTE bApplyOn,
		long lApplyValue,
		DWORD dwFlag,
		long lDuration,
		long lSPCost,
		bool bOverride,
		bool IsCube = false,
#if defined(DBE_REAL_TIME_AFFECTS)
		bool bIsRealTime = false
#endif // DBE_REAL_TIME_AFFECTS
	);