// Search for:

ACMD(do_drop_item);

// Add below:

#if defined(DBE_REAL_TIME_AFFECTS)
ACMD(do_test_realtime_affects);
#endif // DBE_REAL_TIME_AFFECTS

// Search for:

struct command_info cmd_info[] =

// Add at the end of the list, 

#if defined(DBE_REAL_TIME_AFFECTS)
	{ "test_realtime_affects", do_test_realtime_affects, 0, POS_DEAD, GM_IMPLEMENTOR },
#endif // DBE_REAL_TIME_AFFECTS

// !!!! WARNING !!!!
// The last entry of the list must be:

	{ "\n", NULL, 0, POS_DEAD, GM_IMPLEMENTOR } /* Surely this should be the last one. */ 

// So add the test command, above that line