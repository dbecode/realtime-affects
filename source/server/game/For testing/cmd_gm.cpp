// Add at the end of the file:

#if defined(DBE_REAL_TIME_AFFECTS)
ACMD(do_test_realtime_affects)
{
	if (!ch)
		return;

	ch->ChatPacket(CHAT_TYPE_INFO, "Adding AFFECT_BLEND_POTION_5 with bIsRealTime= true for 1 hour");
	ch->AddAffect(AFFECT_BLEND_POTION_5, 95, 200, 0, 3600, 0, true, false, true);
}
#endif // DBE_REAL_TIME_AFFECTS