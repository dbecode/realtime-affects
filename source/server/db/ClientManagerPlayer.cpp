// Modify the function

void CClientManager::QUERY_PLAYER_LOAD as it follow:

// Search for:
// from affect%s
// You should have 3 entries:

// ### 1st entry ###

			// Affect
			snprintf(szQuery, sizeof(szQuery),
				"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost` FROM affect%s WHERE `dwPID` = %d",
				GetTablePostfix(), pTab->id);

// Make it looks like this (The original query is the same as before, just split in multiple lines):

			// Affect
			snprintf(szQuery, sizeof(szQuery),
				"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost`, "
#if defined(DBE_REAL_TIME_AFFECTS)
				"`bIsRealTime` "
#endif // DBE_REAL_TIME_AFFECTS
				"FROM affect%s WHERE `dwPID` = %d",
				GetTablePostfix(), pTab->id);
				

// ### 2nd entry ###

			snprintf(szQuery, sizeof(szQuery),
				"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost` FROM affect%s WHERE `dwPID` = %d",
				GetTablePostfix(), pTab->id);

// Make it looks like this (The original query is the same as before, just split in multiple lines):

			snprintf(szQuery, sizeof(szQuery),
				"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost`, "
#if defined(DBE_REAL_TIME_AFFECTS)
				"`bIsRealTime` "
#endif // DBE_REAL_TIME_AFFECTS
				"FROM affect%s WHERE `dwPID` = %d",
				GetTablePostfix(), pTab->id);


// ### 3rd entry ###

		snprintf(queryStr, sizeof(queryStr),
			"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost` FROM affect%s WHERE `dwPID` = %d",
			GetTablePostfix(), packet->player_id);

// Make it looks like this (The original query is the same as before, just split in multiple lines):

		snprintf(queryStr, sizeof(queryStr),
			"SELECT `dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost`, "
#if defined(DBE_REAL_TIME_AFFECTS)
			"`bIsRealTime` "
#endif // DBE_REAL_TIME_AFFECTS
			"FROM affect%s WHERE `dwPID` = %d",
			GetTablePostfix(), packet->player_id);


/*=============================================================================================================================*/


// Now we are going to move to the next function. Search for:

void CClientManager::RESULT_AFFECT_LOAD(CPeer* peer, MYSQL_RES* pRes, DWORD dwHandle, DWORD dwRealPID)

// Inside this function, search for:

		str_to_number(r.lSPCost, row[6]);

// Add below:

#if defined(DBE_REAL_TIME_AFFECTS)
		str_to_number(r.bIsRealTime, row[7]);
#endif // DBE_REAL_TIME_AFFECTS


/*=============================================================================================================================*/


// Finally, we have to modify:

void CClientManager::QUERY_ADD_AFFECT(CPeer* peer, TPacketGDAddAffect* p)

// Inside this function, change the first query from:

	snprintf(queryStr, sizeof(queryStr),
		"REPLACE INTO affect%s (`dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost`) "
		"VALUES(%u, %u, %u, %ld, %u, %ld, %ld)",
		GetTablePostfix(),
		p->dwPID,
		p->elem.dwType,
		p->elem.bApplyOn,
		p->elem.lApplyValue,
		p->elem.dwFlag,
		p->elem.lDuration,
		p->elem.lSPCost);

// Make it looks like this (The original query is the same as before, just split in multiple lines):

	snprintf(queryStr, sizeof(queryStr),
		"REPLACE INTO affect%s ("
		"`dwPID`, `bType`, `bApplyOn`, `lApplyValue`, `dwFlag`, `lDuration`, `lSPCost`,"
#if defined(DBE_REAL_TIME_AFFECTS)
		"`bIsRealTime`"
#endif // DBE_REAL_TIME_AFFECTS
		") "
		"VALUES("
		"%u, %u, %u, %ld, %u, %ld, %ld, "
#if defined(DBE_REAL_TIME_AFFECTS)
		"%u"
#endif // DBE_REAL_TIME_AFFECTS
		")",
		GetTablePostfix(),
		p->dwPID,
		p->elem.dwType,
		p->elem.bApplyOn,
		p->elem.lApplyValue,
		p->elem.dwFlag,
		p->elem.lDuration,
		p->elem.lSPCost,
#if defined(DBE_REAL_TIME_AFFECTS)
		p->elem.bIsRealTime
#endif // DBE_REAL_TIME_AFFECTS
	);